# README #

### Overview ###

This is the Djangnado-websockets demonstration application that I created in order to make my talk at the SyDjango meetup of 29th May 2014 more interesting.

It showcases the use of websockets to implement a development progress status board for a fictitious software team, with each development task being represented as a coloured bar in a graph, and real time updates being made from an admin page.

### Recent updates ###
As a result of some of the great questions I got on the night I have started making some changes.
The first one is that the update page is now websocket enabled, and all the red and green buttons send a websocket message instead of calling a HTTP url. You will notice that there is now much less lag when you click on one of these buttons. Also this means that there are now two different types of listener, as the update page is itself a listener. To see this in action open up the status update page in two separate windows and then click the update buttons in one (you will need to be logged in, see 'Authentication' below). You will see that the numbers shown to the left of the buttons update in both windows. See the section below on 'Listener types' for more information. 

The second major change is that I have added login authentication. This makes use of the built in Django authentication system and can be used to determine whether websocket messages come from a logged in user or not. How this works is explained in the 'Authentication' section below.

### Listener types ###
Originally there was only one kind of server message, and that consisted of a chunk of svg code that was expected to be set as the innerHTML of some html placeholder tag (a div with id svg_section in this example). Since the layout of the status update page is quite different, it needs the data in another format. 

So a second message type has been defined and this basically consists of a dictionary containing the task names as the keys and the percent complete as the attached values. To send over a websocket connection it is encoded as JSON and then passed to the 'send' function. Note that when using the SockJS library it is not necessary to explicitly do the JSON encoding and decoding as this done automatically by the library.

A client page that wants to be informed of changes to the status can send a 'subscribe' request to the server. This consists of a string containing two words, the first word is 'subscribe' and the second word is either 'svg' or 'numbers'. The value of the second word is stored as an attribute of the BoardSocket instance. When it comes time to send the status updates to the list of listeners, this attribute can be interrogated, and then the appropriate message type (either the svg text or the JSON dictionary) is sent to each of the listeners.

### Authentication ###
When a websocket connection is initiated by the web browser, any cookies are included with the connection request. These cookies are then available to the 'on_open' handler function via the 'info' argument. 

A check is made to see if the 'session_id' cookie exists, and if so its value is extracted and stored. Using the session_id it is then possible to interrogate the Django session framework to find out information about the user name, or indeed any other information stored in the session. In this case, if no session_id cookie exists then the connection request is rejected. 

Anybody can see the 'board' page containing the colourful bar chart without needing to log in. It is also possible to see the 'update' page without being logged in, but it just won't let you update anything. In order for the buttons to actually do anything you have to log in first. For those trying this out on my scribefriday server, I have set up a user account called guest (password guest) for your convenience.

### Slides ###
Several people have asked for the slides of the talk. These are available as a PDF file in the Downloads section, which you can get to via the bottom icon of the panel on the left.