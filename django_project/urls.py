from django.conf.urls import patterns, include, url
from statusboard.views import status_complete_list

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns(
    '',
    # Examples:
    # url(r'^$', 'django_project.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^status_complete_list/', status_complete_list.as_view(), name='status_complete_list'),
    url(r'^reset_data/', 'statusboard.views.reset_data'),
    url(r'^status_board/', 'statusboard.views.status_board'),
    url(r'^logout/', 'statusboard.views.logout', name='logout'),
    url(r'^$', 'statusboard.views.home_page', name='home'),

    url(r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'admin/login.html'}),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout'),

    url(r'^admin/', include(admin.site.urls)),
)
