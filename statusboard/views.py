from django.shortcuts import render, redirect
import django.contrib.auth
from django.contrib.auth.decorators import login_required
from statusboard import the_board
from django.views.generic import ListView
from models import Activity


# Create your views here.
@login_required
def reset_data(request):
    the_board.reset_data()
    return redirect("/status_complete_list")


def home_page(request):
    return render(request, 'statusboard/index.html')


def logout(request):
    django.contrib.auth.logout(request)
    return redirect('home')


def status_board(request):
    if not 'board' in request.session:
        request.session['board'] = 'x'
        return redirect('/status_board')
    return render(request, 'statusboard/status_board.html')


class status_complete_list(ListView):
    template_name = 'statusboard/status_complete_list.html'
    model = Activity
