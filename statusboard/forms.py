from django import forms

__author__ = 'kevin'

class StatusBoardForm(forms.Form):
    activity = forms.CharField(max_length = 5)
    complete = forms.IntegerField()


