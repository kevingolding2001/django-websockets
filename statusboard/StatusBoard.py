__author__ = 'kevin'

from models import Activity
from django.template.loader import render_to_string


class StatusBoard:
    def __init__(self):
        self.BoardListeners = []
        self.the_svg = self.regenerate_status_svg()
        self.num_dict = self.regenerate_status_numbers()

    def register_listener(self, listener):
        self.BoardListeners.append(listener)

    def deregister(self, listener):
        del self.BoardListeners[listener]

    def doing_well(self, task):
        t = Activity.objects.get(title=task)
        if t.complete <= 99:
            t.complete += 1
        t.save()

        self.refresh_listeners()

    def slipping_behind(self, task):
        t = Activity.objects.get(title=task)
        if t.complete >= 1:
            t.complete -= 1
        t.save()

        self.refresh_listeners()

    def refresh_listeners(self):
        self.the_svg = self.regenerate_status_svg()
        self.num_dict = self.regenerate_status_numbers()
        for l in self.BoardListeners:
            l.refresh()

    def regenerate_status_svg(self):
        bar_list = []

        column = 0

        for sv in Activity.objects.all().order_by('id'):
            bar_list.append(self.status_bar(column, sv.complete, sv.title))
            column += 1

        c = {"status_bars": bar_list}

        return render_to_string('statusboard/bar.html', c)

    def regenerate_status_numbers(self):
        n = {}

        for task in Activity.objects.all():
            n[task.title] = task.complete

        return n

    def reset_data(self):
        Activity.objects.all().delete()
        for (init_title, init_complete) in [["Quote", 100], ["Design", 90], ["Spec", 70], ["Dev", 50],
                                            ["Test", 40], ["Docs", 20], ["Impl", 20]]:
            t = Activity(title=init_title, complete=init_complete)
            t.save()

        self.refresh_listeners()

    @staticmethod
    def status_bar(column, value, label):
        bar_width = 60
        bar_gap = 20

        svg_height = 300

        x = column * (bar_width + bar_gap) + bar_gap
        height = value * 2
        y = svg_height - height - 40

        text_x = x + 15
        text_y = svg_height - height - 50

        label_x = x + 5
        label_y = svg_height - 20

        if value < 25:
            colour = "red"
        elif value < 50:
            colour = "orange"
        elif value < 75:
            colour = "yellow"
        elif value < 99:
            colour = "green"
        else:
            colour = "blue"

        bar_dict = {
            "x": x, "y": y,
            "height": height, "colour": colour,
            "text_x": text_x, "text_y": text_y,
            "value": value, "bar_width": bar_width,
            "label_x": label_x, "label_y": label_y, "label": label
        }

        return bar_dict
