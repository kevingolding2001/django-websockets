from __future__ import print_function
import sockjs.tornado
from statusboard import the_board
from django.contrib.sessions.models import Session


class BoardSocket(sockjs.tornado.SockJSConnection):

    client_fmt = ""
    session_id = None

    def on_open(self, info):
        if 'sessionid' in info.cookies:
            self.session_id = info.cookies['sessionid'].value
            the_board.register_listener(self)
        else:
            self.close()

    def on_message(self, message):
        message_parts = message.split()

        if message_parts[0] == "subscribe":
            self.client_fmt = message_parts[1]

            self.refresh()

        elif message_parts[0] == "doing_well":
            if self.user_logged_in():
                the_board.doing_well(message_parts[1])
        elif message_parts[0] == "slipping_behind":
            if self.user_logged_in():
                the_board.slipping_behind(message_parts[1])

    def on_close(self):
        the_board.deregister(self)

    def refresh(self):
        if self.client_fmt == "svg":
            s = the_board.the_svg
            self.send(s)
        elif self.client_fmt == "numbers":
            n = the_board.num_dict
            self.send(n)

    def user_logged_in(self):
        session = Session.objects.get(session_key=self.session_id)
        uid = session.get_decoded().get('_auth_user_id')
        if uid is None:
            return False
        else:
            return True
