
import tornado.options
import django.core.handlers.wsgi
import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.wsgi
from sockjs.tornado import SockJSRouter, SockJSConnection
from statusboard.BoardSocket import BoardSocket

tornado.options.define('port', type=int, default=8080)

BoardRouter = SockJSRouter(BoardSocket, '/websocket')


def main():
    wsgi_app = tornado.wsgi.WSGIContainer(
        django.core.handlers.wsgi.WSGIHandler())
    tornado_app = tornado.web.Application(
        BoardRouter.urls +
        [
        (r"/static/admin/(.*)", tornado.web.StaticFileHandler, {"path": "./static/admin"}),
        (r"/static/(.*)", tornado.web.StaticFileHandler, {"path": "./static"}),
        ('.*', tornado.web.FallbackHandler, dict(fallback=wsgi_app)),
        ],
        debug=True
    )
    server = tornado.httpserver.HTTPServer(tornado_app)
    server.listen(tornado.options.options.port)
    tornado.ioloop.IOLoop.instance().start()


if __name__ == '__main__':
    main()
